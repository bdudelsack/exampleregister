package com.fhflensburg.register.api;

import android.content.Context;

import retrofit.RestAdapter;

/**
 * Author: Dmitri Hammernik
 * Date: 30.11.15
 * Time: 11:30
 */
public class ApiServiceFactory implements IApiServiceFactory {
    private RestAdapter restAdapter;
    private com.fhflensburg.register.api.LocalJsonClient localJsonClient;
    private String scenario = "";

    public ApiServiceFactory(Context context) {
        this.localJsonClient = new com.fhflensburg.register.api.LocalJsonClient(context);
        this.restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.testserver.com")
                .setClient(localJsonClient)
                .build();
    }

    @Override
    public ApiUser createUserApi() {
        return this.restAdapter.create(ApiUser.class);
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
        this.localJsonClient.setScenario(scenario);
    }
}
