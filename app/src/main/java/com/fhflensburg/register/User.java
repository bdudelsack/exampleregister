package com.fhflensburg.register;

/**
 * Author: Dmitri Hammernik
 * Date: 30.11.15
 * Time: 11:18
 */
public class User {
    String email;
    String password;
    String firstname;
    String lastname;

    public User(String email, String password, String firstname, String lastname) {
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
