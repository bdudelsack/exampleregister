package com.fhflensburg.register;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fhflensburg.register.api.ApiException;
import com.fhflensburg.register.api.ApiServiceFactory;
import com.fhflensburg.register.api.ErrorCodes;
import com.fhflensburg.register.api.IApiServiceFactory;


/**
 * A register screen that offers register via email/password.
 */
public class RegisterActivity extends AppCompatActivity {
    private static final String API_URL = "https://api.testserver.com";
    private UserRegisterTask mAuthTask = null;
    private IApiServiceFactory mApiServiceFactory;
    private RegisterService mRegisterService;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private EditText mFirstnameView;
    private EditText mLastNameView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initAuthService();

        // Set up the register form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_register_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        mFirstnameView = (EditText)findViewById(R.id.firstname);
        mLastNameView = (EditText)findViewById(R.id.lastname);
    }

    private void initAuthService() {
        if(mRegisterService == null) {
            mRegisterService = new RegisterService(getApiServiceFactory());
        }
    }

    public IApiServiceFactory getApiServiceFactory() {
        if(mApiServiceFactory == null) {
            mApiServiceFactory = new ApiServiceFactory(this);
        }

        return mApiServiceFactory;
    }

    /**
     * Attempts to sign in or register the account specified by the register form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual register attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the register attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String firstname = mFirstnameView.getText().toString();
        String lastname = mLastNameView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(firstname) && !isNameValid(firstname)) {
            mFirstnameView.setError(getString(R.string.error_invalid_firstname));
            focusView = mFirstnameView;
            cancel = true;
        }

        if (!TextUtils.isEmpty(lastname) && !isNameValid(lastname)) {
            mLastNameView.setError(getString(R.string.error_invalid_lastname));
            focusView = mLastNameView;
            cancel = true;
        }

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user register attempt.
            showProgress(true);
            mAuthTask = new UserRegisterTask(email, password, firstname, lastname, mRegisterService);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isNameValid(String name) {
        return name.length() >= 3;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the register form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous register/registration task used to authenticate
     * the user.
     */
    public class UserRegisterTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private final String mFirstname;
        private final String mLastname;
        private final RegisterService mRegisterService;
        private int errorCode;

        UserRegisterTask(String email, String password, String firstname, String lastname, RegisterService registerService) {
            mEmail = email;
            mPassword = password;
            mFirstname = firstname;
            mLastname = lastname;
            mRegisterService = registerService;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                mRegisterService.register(mEmail, mPassword, mFirstname, mLastname);
            } catch (ApiException e) {
                errorCode = e.getErrorCode();
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (!success) {
                if(errorCode == ErrorCodes.WRONG_EMAIL) {
                    mEmailView.setError(getString(R.string.error_invalid_email));
                    mEmailView.requestFocus();
                } else if(errorCode == ErrorCodes.WRONG_PASSWORD) {
                    mPasswordView.setError(getString(R.string.error_invalid_password));
                    mPasswordView.requestFocus();
                } else if(errorCode == ErrorCodes.EMAIL_ALREADY_EXISTS) {
                    mEmailView.setError(getString(R.string.error_email_exists));
                    mEmailView.requestFocus();
                } else if(errorCode == ErrorCodes.WRONG_FIRSTNAME) {
                    mFirstnameView.setError(getString(R.string.error_invalid_firstname));
                    mFirstnameView.requestFocus();
                } else if(errorCode == ErrorCodes.WRONG_LASTNAME) {
                    mLastNameView.setError(getString(R.string.error_invalid_lastname));
                    mLastNameView.requestFocus();
                }
            } else {
                startActivity(new Intent(RegisterActivity.this, SecondActivity.class));
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

