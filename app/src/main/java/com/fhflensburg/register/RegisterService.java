package com.fhflensburg.register;

import com.fhflensburg.register.api.ApiException;
import com.fhflensburg.register.api.ApiUser;
import com.fhflensburg.register.api.RegisterResponse;
import com.fhflensburg.register.api.ErrorCodes;
import com.fhflensburg.register.api.IApiServiceFactory;

/**
 * Author: Dmitri Hammernik
 * Date: 30.11.15
 * Time: 11:19
 */
public class RegisterService {
    private IApiServiceFactory apiServiceFactory;

    public RegisterService(IApiServiceFactory apiServiceFactory) {
        this.apiServiceFactory = apiServiceFactory;
    }

    public User register(String email, String password, String firstname, String lastname) throws ApiException {
        ApiUser apiUser = this.apiServiceFactory.createUserApi();
        RegisterResponse response = null;

        try {
            response = apiUser.register(email, password, firstname, lastname);
        } catch (Exception e) {
            throw new ApiException(ErrorCodes.API_ERROR);
        }

        if(response == null || response.getStatus() >= 400) {
            throw new ApiException(response.getCode());
        }

        return new User(email, password, firstname, lastname);
    }
}
