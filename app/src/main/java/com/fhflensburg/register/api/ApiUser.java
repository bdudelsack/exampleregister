package com.fhflensburg.register.api;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Author: Dmitri Hammernik
 * Date: 30.11.15
 * Time: 11:11
 */
public interface ApiUser {
    @FormUrlEncoded
    @POST("/user/register")
    RegisterResponse register(@Field("email") String email, @Field("password")String password, @Field("firstname") String firstname, @Field("lastname") String lastname);
}
