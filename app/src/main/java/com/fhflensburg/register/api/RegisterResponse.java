package com.fhflensburg.register.api;

/**
 * Author: Dmitri Hammernik
 * Date: 30.11.15
 * Time: 12:17
 */
public class RegisterResponse {
    private int status;
    private int code;
    private String message;
    private String content;

    public RegisterResponse(int statusCode, int errorCode, String message, String content) {
        this.status = statusCode;
        this.code = errorCode;
        this.message = message;
        this.content = content;
    }

    public int getStatus() {
        return status;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getContent() {
        return content;
    }
}
