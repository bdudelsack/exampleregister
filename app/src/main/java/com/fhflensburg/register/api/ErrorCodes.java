package com.fhflensburg.register.api;

/**
 * Author: Dmitri Hammernik
 * Date: 30.11.15
 * Time: 12:19
 */
public class ErrorCodes {
    public static final int WRONG_EMAIL = -1;
    public static final int WRONG_PASSWORD = -2;
    public static final int WRONG_FIRSTNAME = -3;
    public static final int WRONG_LASTNAME = -4;
    public static final int EMAIL_ALREADY_EXISTS = -5;

    public static final int API_ERROR = -100;
}
