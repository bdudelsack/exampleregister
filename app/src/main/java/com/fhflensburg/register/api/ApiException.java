package com.fhflensburg.register.api;

/**
 * Author: Dmitri Hammernik
 * Date: 30.11.15
 * Time: 13:57
 */
public class ApiException extends Exception {
    private int errorCode;

    /**
     * Constructs a new {@code Exception} that includes the current stack trace.
     */
    public ApiException(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
