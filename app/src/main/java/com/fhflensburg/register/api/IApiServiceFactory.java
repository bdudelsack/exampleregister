package com.fhflensburg.register.api;

/**
 * Author: Dmitri Hammernik
 * Date: 30.11.15
 * Time: 11:30
 */
public interface IApiServiceFactory {
    ApiUser createUserApi();
}
