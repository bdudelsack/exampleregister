package com.fhflensburg.register.api;

import android.content.Context;

import retrofit.RestAdapter;

/**
 * Author: Dmitri Hammernik
 * Date: 30.11.15
 * Time: 11:30
 */
public class ApiServiceFactory implements IApiServiceFactory {
    private RestAdapter restAdapter;

    public ApiServiceFactory(Context context) {
        this.restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://192.168.40.144:8082")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
    }

    @Override
    public ApiUser createUserApi() {
        return this.restAdapter.create(ApiUser.class);
    }
}
