package com.fhflensburg.register;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.fhflensburg.register.api.ApiServiceFactory;
import com.fhflensburg.register.api.IApiServiceFactory;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertNotNull;

/**
 * Author: Dmitri Hammernik
 * Date: 30.11.15
 * Time: 12:31
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class RegisterTest {
    @Rule
    public ActivityTestRule<RegisterActivity> activityRule = new ActivityTestRule<>(RegisterActivity.class);

    private RegisterActivity registerActivity;

    @Before
    public void setUp() throws Exception {
        registerActivity = activityRule.getActivity();
    }

    @Test
    public void testPreconditions() throws Exception {
        //assert
        assertNotNull(registerActivity);
    }

    @Test
    public void testRegisterSuccess() throws Exception {
        //act
        onView(withId(R.id.email)).perform(typeText("test@test.com"));
        onView(withId(R.id.password)).perform(typeText("testpassword"));
        onView(withId(R.id.firstname)).perform(typeText("Hans"));
        onView(withId(R.id.lastname)).perform(typeText("Wurst"));
        closeSoftKeyboard();
        onView(withId(R.id.email_register_button)).perform(click());

        //assert
        onView(withId(R.id.second_activity_view)).check(matches(isCompletelyDisplayed()));
    }

    @Test
    public void testWrongPassword() throws Exception {
        //arrange
        IApiServiceFactory apiServiceFactory = registerActivity.getApiServiceFactory();
        ((ApiServiceFactory)apiServiceFactory).setScenario("error_password");

        //act
        onView(withId(R.id.email)).perform(typeText("test@test.com"));
        onView(withId(R.id.password)).perform(typeText("testpassword"));
        onView(withId(R.id.firstname)).perform(typeText("Hans"));
        onView(withId(R.id.lastname)).perform(typeText("Wurst"));
        closeSoftKeyboard();
        onView(withId(R.id.email_register_button)).perform(click());

        //assert
        onView(withId(R.id.password)).check(matches(ViewMatchers.hasErrorText("This password is too short")));
    }

    @Test
    public void testWrongEmail() throws Exception {
        //arrange
        IApiServiceFactory apiServiceFactory = registerActivity.getApiServiceFactory();
        ((ApiServiceFactory)apiServiceFactory).setScenario("error_email");

        //act
        onView(withId(R.id.email)).perform(typeText("test@test.com"));
        onView(withId(R.id.password)).perform(typeText("testpassword"));
        onView(withId(R.id.firstname)).perform(typeText("Hans"));
        onView(withId(R.id.lastname)).perform(typeText("Wurst"));
        closeSoftKeyboard();
        onView(withId(R.id.email_register_button)).perform(click());

        //assert
        onView(withId(R.id.email)).check(matches(ViewMatchers.hasErrorText("This email address is invalid")));
    }

    @Test
    public void testEmailExists() throws Exception {
        //arrange
        IApiServiceFactory apiServiceFactory = registerActivity.getApiServiceFactory();
        ((ApiServiceFactory)apiServiceFactory).setScenario("error_email_exists");

        //act
        onView(withId(R.id.email)).perform(typeText("test@test.com"));
        onView(withId(R.id.password)).perform(typeText("testpassword"));
        onView(withId(R.id.firstname)).perform(typeText("Hans"));
        onView(withId(R.id.lastname)).perform(typeText("Wurst"));
        closeSoftKeyboard();
        onView(withId(R.id.email_register_button)).perform(click());

        //assert
        onView(withId(R.id.email)).check(matches(ViewMatchers.hasErrorText("Email already exists")));
    }
}
